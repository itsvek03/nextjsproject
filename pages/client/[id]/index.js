import React from 'react'
import {useRouter} from 'next/router'

export default function ClientName() {
    const router =useRouter()

    function ProjectLoader(){
        router.push({
            pathname:'/client/[id]/[projectname]',
            query:{id:router.query.id,projectname:`${router.query.id}project1`}
        })
    }

    return (
        <div>
            <h1>This is the Client Info</h1>
            <h1>Load of project {router.query.id}</h1>
            <button onClick={ProjectLoader}>Load</button>
        </div>
    )
}
