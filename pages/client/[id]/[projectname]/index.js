import React from 'react'
import {useRouter} from 'next/router'

/**
 *  [...pathname] folder
 * It is useful when query is like /2020-10-30. It will store as ["2020-10-10"]
 * 
 * if query like 2020/10/10 it will store in array ["2020",10,10]
 * 
*/

export default function Projectname() {
    const router =useRouter()
    // console.log("PROJECT",router.pathname)
    // console.log("PROJECT QUERY",router.query)
    return (
        <div>
            <h1>This is the Project Name File {router.query.id} {router.query.projectname}</h1>
        </div>
    )
}
