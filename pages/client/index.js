import React from 'react'
import Link from 'next/link'

export default function ClientIndex() {
    const clients =[
        {id:"max",name:"Maxilmala"},
        {id:"vivek",name:"Maurya"}
    ]
     // <li key={client.id}>
                        //     <Link href={`/client/${client.id}`}>{client.name}</Link>
                        // </li>
    return (
        <div>
            <h1>This is the Client Index Page</h1>
            <ul>
                {clients.map((client) =>(
                        <li key={client.id}>
                            <Link 
                            href={{
                                pathname: "/client/[id]",
                                query:{ id: client.id },
                            }}>
                                {client.name}
                            </Link>
                        </li>

                    )
                )}
            </ul>
        </div>
    )
}
